//
//  ViewController.swift
//  CameraTest
//
//  Created by Tari Tantona on 06/11/2018.
//  Copyright © 2018 Tari Tantona. All rights reserved.
//

import UIKit
import Alamofire
import TLPhotoPicker
import  Gallery
import Lightbox
import AVFoundation
import AVKit
import ImageSlideshow

class ViewController: UIViewController,TLPhotosPickerViewControllerDelegate, LightboxControllerDismissalDelegate,GalleryControllerDelegate{
    var count: [Int] = []
    var selectedAssets = [TLPHAsset]()
    @IBOutlet weak var imageFrame: ImageSlideshow!
    @IBOutlet weak var hearts: UILabel!
    @IBOutlet weak var likes: UILabel!
    
    
    
    func lightboxControllerWillDismiss(_ controller: LightboxController) {
        
    }
    var linksSource = [InputSource]()
    var gallery: GalleryController!
    let editor: VideoEditing = VideoEditor()
    var currentImage = 0

    func galleryController(_ controller: GalleryController, didSelectImages images: [Image]) {
        LightboxConfig.DeleteButton.enabled = false
        
       // SVProgressHUD.show()
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            //SVProgressHUD.dismiss()
            self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
            
        })
        controller.dismiss(animated: true, completion: nil)
    }
    
    func galleryController(_ controller: GalleryController, didSelectVideo video: Video) {
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
        
        
        editor.edit(video: video) { (editedVideo: Video?, tempPath: URL?) in
            DispatchQueue.main.async {
                if let tempPath = tempPath {
                    let controller = AVPlayerViewController()
                    controller.player = AVPlayer(url: tempPath)
                    
                    self.present(controller, animated: true, completion: nil)
                }
            }
        }
    }
    
    func galleryController(_ controller: GalleryController, requestLightbox images: [Image]) {
        LightboxConfig.DeleteButton.enabled = false
        
        
        Image.resolve(images: images, completion: { [weak self] resolvedImages in
            
            self?.showLightbox(images: resolvedImages.compactMap({ $0 }))
        })
        
    }
    
    func galleryControllerDidCancel(_ controller: GalleryController) {
        controller.dismiss(animated: true, completion: nil)
        gallery = nil
    }
    
    func showLightbox(images: [UIImage]) {
        guard images.count > 0 else {
            return
        }
        count.removeAll()
        for i  in images {
            linksSource.append(ImageSource(image: i))
            count.append(0)
        }
        
    
        
        imageFrame.setImageInputs(linksSource)
        let lightboxImages = images.map({ LightboxImage(image: $0) })
        let lightbox = LightboxController(images: lightboxImages, startIndex: 0)
        lightbox.dismissalDelegate = self
        linksSource.removeAll()
        
        gallery.present(lightbox, animated: true, completion: nil)
        
        
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageFrame.pageIndicatorPosition = .init(horizontal: .left(padding: 5), vertical: .customBottom(padding: 5))
        imageFrame.contentScaleMode = UIView.ContentMode.scaleAspectFill
        //imageFrame.subviews
        
        let pageNum = LabelPageIndicator();
        let pageControl = UIPageControl()
        Config.Grid.FrameView.borderColor = UIColor(red: 1/255, green: 1/255, blue: 236/255, alpha: 0.9)
        pageControl.currentPageIndicatorTintColor = UIColor.lightGray
        pageControl.pageIndicatorTintColor = UIColor.black
        
        pageNum.textColor = UIColor.white
        pageNum.font = pageNum.font.withSize(12)
        pageNum.backgroundColor = UIColor.black.withAlphaComponent(0.9)
        pageNum.layer.masksToBounds = true
        pageNum.layer.cornerRadius = 3
        
        imageFrame.pageIndicator = pageNum
        
        // optional way to show activity indicator during image load (skipping the line will show no activity indicator)
        imageFrame.activityIndicator = DefaultActivityIndicator()
        imageFrame.currentPageChanged = { page in
            print("current page:", page)
            
            self.hearts.text = String(self.count[page])
        }
        
        // can be used with other sample sources as `afNetworkingSource`, `alamofireSource` or `sdWebImageSource` or `kingfisherSource`
        
        
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(ViewController.didTap))
        imageFrame.addGestureRecognizer(recognizer)
        let likeGest = UILongPressGestureRecognizer(target: self, action: #selector(ViewController.didHold))
        
        imageFrame.addGestureRecognizer(recognizer)
        imageFrame.addGestureRecognizer(likeGest)
        
        Gallery.Config.VideoEditor.savesEditedVideoToLibrary = true
        Gallery.Config.Camera.imageLimit = 5
        
        
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @objc func didHold(gestureRecognizer: UILongPressGestureRecognizer) {
        let myindex = imageFrame.currentPage
        guard count.count > 0 else {
            return
        }
        if gestureRecognizer.state == .began {
            count[myindex]+=1
            for i in count{
                print("\(i) " )
            }
            hearts.text = String(count[myindex])
        }
        
        
        let alert = UIAlertController(title: "Alert", message: "\(count[myindex])  likes", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        
        
        self.present(alert, animated: true)
    }
    
    @objc func didTap() {
        let fullScreenController = imageFrame.presentFullScreenController(from: self)
        // set the activity indicator for full screen controller (skipping the line will show no activity indicator)
        fullScreenController.slideshow.activityIndicator = DefaultActivityIndicator(style: .white, color: nil)
    }
    
    @IBAction func cameraButton(_ sender: Any) {
        /*let viewController = TLPhotosPickerViewController()
        viewController.delegate = self
        var configure = TLPhotosPickerConfigure()
        //configure.nibSet = (nibName: "CustomCell_Instagram", bundle: Bundle.main) // If you want use your custom cell..
        configure.maxSelectedAssets = 5
        configure.numberOfColumn = 3
        
        viewController.configure = configure
        
        viewController.selectedAssets = self.selectedAssets
        
        
        self.present(viewController, animated: true, completion: nil)
        */
        gallery = GalleryController()
        gallery.delegate = self
        
        present(gallery, animated: true, completion: nil)

    }
    @IBAction func resetBtn(_ sender: Any) {
        gallery.dismiss(animated: true, completion: nil)
        
    }
    

}

